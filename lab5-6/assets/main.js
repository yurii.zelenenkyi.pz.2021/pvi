const socket=io()
const messages=document.querySelector('.message-list')
const messages_notification=document.querySelector('.notification-list')
const chat_container=document.querySelector('.container')
const login_form=document.querySelector('.login_form')
const form= document.querySelector('.form')
const form2= document.querySelector('.add_form')
const input= document.querySelector('.input')
const input2= document.querySelector('.input_new')
const nameUser=document.querySelector('.nameUser')
var user_list = document.getElementById('user-list');

chat_container.style.display='none';
var userName =""
// const userName=prompt('Ваше ім\'я')
// 

users = [];

form2.addEventListener('submit',(e)=>
{
    e.preventDefault()
    if(input2.value)
    {
        userName=input2.value

        socket.emit('new conn',
        {
            name:input2.value
        })
        nameUser.innerHTML=userName;
        input2.innerHTML=""
        login_form.style.display='none';
        chat_container.style.display='flex';
    }
})

form.addEventListener('submit',(e)=>
{
    e.preventDefault()
    var checkboxes = user_list.querySelectorAll('input[type="checkbox"]')
    users =[]
        checkboxes.forEach(function(checkbox) {
             if (checkbox.checked) {
                    var listItemText = checkbox.parentNode.textContent.trim()
                    users.push(listItemText)
                }
                
        });
    if(input.value && users.length)
    {
        
        //var selectedItems = [];
        const currentTime = new Date();
        var myObject = {
            message: input.value,
            sender: userName,
            receivers: users,
            timestamp: currentTime
          };
        //console.log(users)
        // console.log(myObject.sender)
        // console.log(myObject.message)
        // console.log(myObject.receivers)
        input.value=""
        socket.emit('chat message',myObject)
        
    }

})

socket.on('chat message',(data)=>
    {
        const item=document.createElement('li')
        //item.innerHTML=`(${data.sender}):${data.message} (${data.receivers})`
        console.log(data.timestamp)
        console.log(data.message)
        const date = new Date(data.timestamp);
      const hours = date.getHours();
      const minutes = date.getMinutes();
      const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
        item.innerHTML=`${data.sender} : ${data.message} {${formattedTime}}`
        // console.log("fdghrt");
        // console.log(data.sender)
        // console.log(data.message)
        // console.log(data.receivers)
        messages.appendChild(item)
        messages.scrollTop = messages.scrollHeight
       // window.scrollTo(0,document.body.scrollHeight)
    })

    socket.on('new conn',(data)=>
    {   
        if(data.name!=userName)
        {
        const item=document.createElement('li')
        item.innerHTML=`${data.name}`
        var checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        item.appendChild(checkbox)
        // console.log("fdghrt");
        // console.log(data.sender)
        // console.log(data.message)
        // console.log(data.receivers)
        
        user_list.appendChild(item)
        window.scrollTo(0,document.body.scrollHeight)
        }
    })
    socket.on('get users',(data)=>
    {   
        if(data.users.length)
        {
            data.users.forEach(user => {
                if(user.name!=userName)
                {
                const listItem = document.createElement('li');
                listItem.textContent = user.name;
                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                listItem.appendChild(checkbox)
                user_list.appendChild(listItem);
                }
              });
        }
    })
    socket.on('get mess',(data)=>
    {   
        if(data.mess.length)
        {
            const messages_not=data.mess
            const filteredMessages = messages_not.filter(message_not =>
                message_not.receivers.includes(userName)
              );
              
              const lastThreeMessages = filteredMessages.slice(-3);
              
              console.log(lastThreeMessages);

              lastThreeMessages.forEach(mes => {
                
                const listItem = document.createElement('li');
                const date = new Date(mes.timestamp);
                const hours = date.getHours();
                const minutes = date.getMinutes();
                const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
                listItem.textContent =mes.sender+" : "+ mes.message+`{${formattedTime}}`;
                
                messages_notification.appendChild(listItem);
                //messages_notification.scrollTop = messages.scrollHeight
                
              });

            data.mess.forEach(mes => {
                
                const listItem = document.createElement('li');
                const date = new Date(mes.timestamp);
                const hours = date.getHours();
                const minutes = date.getMinutes();
                const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
                listItem.textContent =mes.sender+" : "+ mes.message+`{${formattedTime}}`;
                
                messages.appendChild(listItem);
                messages.scrollTop = messages.scrollHeight
                
              });
        }
    })

    const notificationIcon = document.querySelector('.notification-icon');
const notificationDropdown = document.querySelector('.notification-dropdown');

notificationIcon.addEventListener('click', function() {
  notificationDropdown.classList.toggle('active');
});