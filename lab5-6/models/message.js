const mongoose=require('mongoose')
const Schema=mongoose.Schema


const messageSchema=new Schema({
    message:{
        type:String,
        required:true
    },
    sender:{
        type:String,
        required:true
    },
    receivers:
    {
        type:[String],
        required:true
    },
    timestamp: {
        type: Date,
        default: Date.now
      }
});

const MessageS=mongoose.model('MessageS',messageSchema)

module.exports=MessageS