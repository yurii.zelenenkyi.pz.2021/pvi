const express=require("express")
const app=express()
const http=require("http").createServer(app)
const io=require("socket.io")(http)
const UserS=require('./models/user.js')
const MessageS=require('./models/message.js')
const mongoose = require('mongoose');
const { Console } = require("console")

const uri = 'mongodb://mongo:bCHwd4FssfJj2hgKS4ad@containers-us-west-42.railway.app:5486'; 
// Підключення до бази даних
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

// Перевірка статусу підключення
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Помилка підключення до MongoDB:'));
db.once('open', function() {
  console.log('Підключено до бази даних MongoDB!');
});


app.get('/',(req,res)=>
{
    res.sendFile(__dirname+"/index.html")
})
app.use(express.static(__dirname+'/assets'))

const connectedUsers = {};


io.on('connection',(socket)=>
{
    console.log("join")
    
    socket.on('new conn',(data)=>
    {
        //const UserS = mongoose.model('UserS', userSchema);
        const name=data.name
        connectedUsers[socket.id] = name;
        UserS.findOne({ name: name })
        .then(user => {
        if (user) {
            console.log('Користувач із заданим ім\'ям знайдений');
                UserS.find()
                .then(all_users=>{
                    socket.emit('get users', {
                        users:all_users
                    })
                })
            
            //вивести повідомлення
            MessageS.find({
                $or: [
                    { receivers: { $in: name } },
                    { sender: { $in: name } }
                  ]
                })
            .sort({ timestamp: 1 })
            .then(all_mess => {
                //console.log(all_mess);
                // Обробляйте отримані повідомлення за потребою
                socket.emit('get mess', {
                    mess:all_mess
                })
            })
            .catch(error => {
                console.error('Помилка при отриманні повідомлень:', error);
            });    



            // Виконувати дії знайденого користувача
        } else {
            console.log('Користувач із заданим ім\'ям не знайдений');
      // Виконувати інші дії, які потрібні, якщо користувач не знайдений
        const user=new UserS({name})
        user.save()
        .then(()=>{
            console.log("add to db")
            UserS.find()
                .then(all_users=>{
                    socket.emit('get users', {
                        users:all_users
                    })
                })
        })
        }

        })
        .catch(error => {
            console.error('Помилка при перевірці користувача:', error);
        });

    })
    socket.on('chat message',(data)=>
    {
        console.log(data.message)
        console.log(data.timestamp)
        socket.emit('chat message', {
                message:data.message,
                sender:data.sender,
                receivers:data.receivers,
                timestamp:data.timestamp
            })

        const sockets = Object.keys(connectedUsers);
        
        const message=data.message
         const sender=data.sender
         const receivers=data.receivers
        // Перебір усіх сокетів
        sockets.forEach((socketId) => {
        const userName1 = connectedUsers[socketId];
            const users_rec=data.receivers;
            users_rec.forEach((user_r)=>
            {
                            // Перевірка умови userName == "Yura" або userName == "Dima"
                            
                    if (userName1 === user_r) {
                       // console.log("Send");
                        // Відправлення повідомлення до сокету
                        io.to(socketId).emit('chat message', {
                            message:data.message,
                            sender:data.sender,
                            receivers:data.receivers,
                            timestamp:data.timestamp
                        });
                    }
            })
        
        });

        const new_message=new MessageS({message,sender,receivers})
        new_message.save()
        
    })


    socket.on('disconnect', () => {
        // Видалення сокету та імені користувача з масиву
        delete connectedUsers[socket.id];
    })
})
 

http.listen(4000,()=>
{
    console.log("Server start")
})



