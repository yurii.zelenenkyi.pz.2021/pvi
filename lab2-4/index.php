<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        
        <meta name="description" content="My PWA Example">
        <!-- <link rel="manifest" href="/manifest.json"> -->
        <link rel="stylesheet" href="./style.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='https://unpkg.com/css.gg@2.0.0/icons/css/profile.css' rel='stylesheet'>
        <link href='https://unpkg.com/css.gg@2.0.0/icons/css/bell.css' rel='stylesheet'>
        <title>Students</title>
    </head>
    <body>
    
    <header>
       <nav>
        CMS
       </nav> 
       <nav class="nav-1">
        <ul class="menu-main">

            <li class="bell"><a><i class="gg-bell">
                <span class="notif">
                
                </span>
                
            </i></a>
            <ul class="menu-sub" id="sm2">
                  <li><div><i class="gg-profile"></i>Admin</div>
                  <textarea readonly>message</textarea>
                  </li>
                  <li><div><i class="gg-profile"></i>Darth Vader</div>
                  <textarea readonly>message</textarea>
                  </li>
              </ul>
          </li>

            <li id="li1"><a id="log_name"><i class="gg-profile"></i> Guest</a>
                <ul id="ul2" class="menu-sub">
                    <!-- <li><a>Profile</a></li> -->
                    <li id="login_" ><a>Login</a></li>
                </ul>
            </li>
        </ul>
       </nav>

    </header>

        <aside class="aside-div">
            <ul class="aside-list">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Students</a></li>
                <li><a href="#">Tasks</a></li>
            </ul>
        </aside>
        <h3 class="h-3">Students</h3>
        <button class="fa fa-plus" id="btn-plus" ></button>
        <table class="table-1" id="table-1">
            <thead>
            <tr>
            <th><input type="checkbox" disabled></td>
            <th>Group</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Birthday</th>
            <th>Status</th>
            <th>Options</th>
            </tr>
            </thead>
            <tbody>
            <!-- <tr>
                <td><input type="checkbox"><p hidden>1</p></td>
                <td>PZ-23</td>
                <td>Yura Zelenenkyi</td>
                <td>M</td>
                <td>17.02.2004</td>
                <td><div class="base" id="s1" onclick="fun1(this)">
        
                </div></td>
                <td><button class="edit_btn"><i class="fa fa-edit"></i></button><button class="delete-btn"><i class="fa fa-close"></i></button></td>
            </tr> -->
            <!-- <?php
            $conn=new mysqli("localhost","root","","my_sql_pvi");
            $conn->query("SET NAMES 'utf8'");
            $sql = "SELECT * FROM students";
                $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                echo "<tbody>";
            
                // Виведення рядків даних
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["id"] . "</td>"; // Замініть "id" на власні поля таблиці
                    echo "<td>" . $row["f_name"] . "</td>"; // Замініть "назва" на власні поля таблиці
                    echo "<td>" . $row["s_name"] . "</td>"; // Замініть "опис" на власні поля таблиці
                    echo "</tr>";
                }
            
                echo "</tbody>";
            } else {
                echo "<tbody><tr><td colspan='3'>Немає даних в таблиці</td></tr></tbody>";
            }
            $conn->close();
            ?> -->
            </tbody>
        </table>

        <div id="myModal" class="modal">

            
            <div class="modal-content">
              <span class="close">&times;</span>
              <!-- <p>Add a new user?</p>
              <button class="add-new">OK</button> -->
              <h3 id="add_or_edit">Add Student</h3>
              <form id="registerForm" action="/add.php" method="post">
                <div>
                <label for="group">Group </label>
                <select name="group" id="group_id" required>
                    <option value="" disabled selected hidden>Select Group</option>
                    <option value="PZ-21">PZ-21</option>
                    <option value="PZ-22">PZ-22</option>
                    <option value="PZ-23">PZ-23</option>
                    <option value="PZ-24">PZ-24</option>
                    <option value="PZ-25">PZ-25</option>
                    <option value="PZ-26">PZ-26</option>
                    
                </select>
                </div>
                <br>
                <label for="f_name">First name:</label>
                <input type="text" id="f_name" name="f_name" required ><br><br>
                <label for="l_name">Last name:</label>
                <input type="text" id="l_name" name="l_name" required ><br><br>
                
              
                <div>
                    <label for="gender">Gender </label>
                    <select name="gender" id="gender_id" required>
                        <option value="" disabled selected hidden>Select Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        
                        
                    </select>
                    </div>
                    <br>
                    <label for="birthdate">Birthday</label>
                    
                    <input type="date" id="birthdate" name="birthdate" min="1940-01-01" max="2006-01-01" required><br><br>
                    <input type="text" name="id_element" id="id_element" hidden>
                <button type="submit" id="sbutton" >Submit</button>
              </form>
            </div>
            
        </div>

        <div id="myModal2" class="modal">

          <div id="confirm-modal">
            <p>Ви впевнені, що хочете видалити цей рядок?</p>
            <button id="confirm-btn-yes">Так</button>
            <button id="confirm-btn-no">ні</button>
          </div>
        </div>
        <div id="login-modal" class="modal">
            <div class="modal-content">
              <span class="close">&times;</span>
              <form id="login-form" method="POST">
                <label for="username">Login:</label>
                <input type="text" id="username" name="username" required>
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required>
                <button type="submit" id="submit-button">Увійти</button>
              </form>
            </div>
          </div>
          <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="main.js"></script>
    
    <script >
        // if("serviceWorker" in navigator) {
        //     window.addEventListener("load", function() {
        //         navigator.serviceWorker.register("sw.js").then(function(registration) {
        //             console.log("ServiceWorker registration successful with scope: ", registration.scope);
        //         }, function(err) {
        //             console.log("ServiceWorker registration failed: ", err);
        //         });
        //     });
        // }

    </script>
    </body>
</html>