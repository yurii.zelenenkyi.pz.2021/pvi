<?php
// Підключення до бази даних та отримання даних
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_sql_pvi";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Помилка підключення до бази даних: " . $conn->connect_error);
}

$sql = "SELECT * FROM students";
$result = $conn->query($sql);

$data = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }
}

// Повернення даних у форматі JSON
header('Content-Type: application/json');
echo json_encode($data);

// Закриття з'єднання з базою даних
$conn->close();
?>