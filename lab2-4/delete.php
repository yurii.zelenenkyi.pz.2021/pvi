<?php
$data = json_decode(file_get_contents('php://input'), true);
$rowId = $data['id'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_sql_pvi";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Помилка підключення до бази даних: " . $conn->connect_error);
}



// Запит на видалення рядка
$query = "DELETE FROM `students` WHERE `id` = $rowId";

if ($conn->query($query) === TRUE) {
  echo json_encode(['status' => 'success', 'message' => 'Рядок видалено успішно.']);
} else {
  echo json_encode(['status' => 'error', 'message' => 'Помилка видалення рядка: ' . $rowId]);
}

// Закриття з'єднання з базою даних
$conn->close();


?>