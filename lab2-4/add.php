<?php

$fname = $_POST['f_name'];
$lname = $_POST['l_name'];
$group=$_POST['group'];
$gender=$_POST['gender'];
$birthdate=$_POST['birthdate'];

// // Отримання JSON-стрічки з POST-запиту
// $json_str = file_get_contents('php://input');

// // Перетворення JSON-стрічки в асоціативний масив
// $data = json_decode($json_str, true);

// // Виведення отриманих даних
// var_dump($data);

if (empty(trim($fname)) || empty(trim($lname)) || empty(trim($group)) || empty(trim($gender)) || empty(trim($birthdate))) {
    
  
    $response = array('success' => false, 'message' => 'invalid');
    
    echo json_encode($response);
    //echo "good";
    exit();
}
else if (!preg_match('/^[A-Za-z\'\- ]+$/', $fname) || !preg_match('/^[A-Za-z\'\- ]+$/', $lname)) {
  // Помилка: недопустимі символи в імені або прізвищі
  $response = array('success' => false, 'message' => 'Недопустимі символи');
    
    echo json_encode($response);
  
} elseif (strlen($fname) > 50 || strlen($lname) > 50) {
  // Помилка: перевищено обмеження кількості символів
  $response = array('success' => false, 'message' => 'Не більше 50 символі');
    
    echo json_encode($response);
  
} elseif (!preg_match('/^[A-Z]/', $fname) || !preg_match('/^[A-Z]/', $lname)) {
  // Помилка: перша літера має бути великою
  
  $response = array('success' => false, 'message' => 'Перша літера повина бути великою');
    
    echo json_encode($response);
} else {
  
  $servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_sql_pvi";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Помилка підключення до бази даних:" . $conn->connect_error);
}
//$sql = "INSERT INTO `students` (`id`, `f_name`, `s_name`, `date`, `group_`, `gender`) VALUES (NULL, $fname, $lname, $birthdate, $group, $gender);";
$sql = "INSERT INTO `students` (`id`, `f_name`, `s_name`, `date`, `group_`, `gender`) VALUES (NULL, '$fname', '$lname', '$birthdate', '$group', '$gender');";
if ($conn->query($sql) === FALSE) {
  $response = array('success' => false, 'message' => 'Dont add row to DB');
    
    echo json_encode($response);
} 
$lastInsertedId = mysqli_insert_id($conn);
//$lastInsertedId=10;
$conn->close();
  
  // Якщо дані коректні, обробляємо їх та повертаємо відповідь
  // ...
  $student=array
  (
    'f_name'=>$fname,
    's_name'=>$lname,
    'group_'=>$group,
    'gender'=>$gender,
    'date'=>$birthdate,
    'id'=>$lastInsertedId
  );
  header('Content-Type: application/json');
  //$response = array('success' => true, 'message' => ("dfghnt"));
  echo json_encode($student);

} 

?>